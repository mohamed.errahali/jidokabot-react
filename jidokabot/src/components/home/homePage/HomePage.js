import { Login } from "../login/Login"

export const HomePage = (props) =>{
    return <div  style={
        {
            backgroundImage: "url('jidoka.png')",
            height : "100vh",
            display : "flex" ,
            flexDirection : "column",
        }
    }>

<div style = {{
            display : "flex",
            flexDirection : "row" ,
            justifyContent : "flex-end" ,
            paddingRight : "3px",
            color : "blue"
        }} ><Login /> </div>
        <div style={{
            marginLeft : "60px",
            display : "flex" ,
            flexDirection : "column",
            alignContent : "center" ,
            paddingTop : "30vh",
            textAlign : "center",
            color : "blue"
            
        }}>
            <h1>JidokaBot </h1>
            <hr/>
            <h2>TRELLO'S BEST FRIEND TO SUCCEED IN YOUR SCRUM PROJECT</h2>
            </div>
        </div>
}
