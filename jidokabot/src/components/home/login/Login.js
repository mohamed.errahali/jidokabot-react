import TrelloClient from "react-trello-client" ;
import {Navigate,useNavigate} from "react-router-dom" ;
export const Login = (props) =>{
    const navigate = useNavigate();
    return <TrelloClient
    apiKey="e193dd7ec1a31738d605f78edd0e1180" // Get the API key from https://trello.com/app-key/
    clientVersion={1} // number: {1}, {2}, {3}
    apiEndpoint="https://api.trello.com" // string: "https://api.trello.com"
    authEndpoint="https://trello.com" // string: "https://trello.com"
    intentEndpoint="https://trello.com" // string: "https://trello.com"
    authorizeName="React Trello Client" // string: "React Trello Client"
    authorizeType="popup" // string: popup | redirect
    authorizePersist={true} 
    authorizeInteractive={true}
    authorizeScopeRead={false} // boolean: {true} | {false}
    authorizeScopeWrite={true} // boolean: {true} | {false}
    authorizeScopeAccount={true} // boolean: {true} | {false}
    authorizeExpiration="never" // string: "1hour", "1day", "30days" | "never"
    authorizeOnSuccess={() =>  navigate("/features") } // function: {() => console.log('Login successful!')}
    authorizeOnError={() => console.log('Login error!')} // function: {() => console.log('Login error!')}
    autoAuthorize={false} // boolean: {true} | {false}
    authorizeButton={true} // boolean: {true} | {false}
    buttonStyle="flat" // string: "metamorph" | "flat"
    buttonColor="light" // string: "green" | "grayish-blue" | "light"
    buttonText="Continue avec trello" // string: "Login with Trello"
/>
}