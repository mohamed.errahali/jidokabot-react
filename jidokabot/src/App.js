import { HomePage, Login } from './components/home';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ListProjets, PageFeatures } from './components/features';

function 
App() {
  return (
    <div>
       <BrowserRouter>
        <Routes>
          <Route path="/" element={  <HomePage/>} /> 
          <Route path="/login" element={  <Login/>} /> 
          <Route path="/features" element={  <PageFeatures/>} /> 
          <Route path="/select_project" element={ <ListProjets/>} /> 
          
        </Routes> 
        </BrowserRouter> 
        
    </div>
  );
}

export default App;
